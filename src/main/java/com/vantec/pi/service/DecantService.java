package com.vantec.pi.service;

import com.vantec.pi.model.DecantF2;
import com.vantec.pi.model.DecantRequest;

public interface DecantService {

	String createDecantHeader(DecantRequest decantRequest);

	Long f2Done(DecantF2 decantF2);


	
}
