package com.vantec.pi.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pi.entity.DocumentStatus;
import com.vantec.pi.entity.InventoryMaster;
import com.vantec.pi.entity.InventoryStatus;
import com.vantec.pi.entity.Location;
import com.vantec.pi.entity.PIBody;
import com.vantec.pi.entity.PIHeader;
import com.vantec.pi.entity.PIReasonCode;
import com.vantec.pi.entity.Part;
import com.vantec.pi.entity.TransactionHistory;
import com.vantec.pi.model.PIBodyRequest;
import com.vantec.pi.model.PIHeaderRequest;
import com.vantec.pi.model.PILocationHeaderRequest;
import com.vantec.pi.model.PIPartHeaderRequest;
import com.vantec.pi.model.ProcessRequest;
import com.vantec.pi.repository.DocumentStatusRepository;
import com.vantec.pi.repository.InventoryMasterRepository;
import com.vantec.pi.repository.InventoryStatusRepository;
import com.vantec.pi.repository.LocationRepository;
import com.vantec.pi.repository.PIBodyRepository;
import com.vantec.pi.repository.PIHeaderRepository;
import com.vantec.pi.repository.PIReasonCodeRepository;
import com.vantec.pi.repository.PartRepository;
import com.vantec.pi.repository.TransactionHistoryRepository;


@Service("PIService")
public class PIServiceImpl implements PIService{
	
	
	@Autowired
	PIHeaderRepository piHeaderRepository;
	
	@Autowired
	PIBodyRepository piBodyRepository;
	
	@Autowired
	InventoryMasterRepository inventoryMasterRepository;
	
	@Autowired
	DocumentStatusRepository documentStatusRepository;
	
	@Autowired
	InventoryStatusRepository inventoryStatusRepository;
	
	@Autowired
	LocationRepository locationRepository;
	
	@Autowired
	TransactionHistoryRepository transactionHistoryRepository;
	
	@Autowired
	PIReasonCodeRepository piReasonCodeRepository;
	
	@Autowired
	PartRepository partRepository;
	


	@Override
	public Boolean createUpdatePIHeader(PIHeaderRequest piHeaderRequest) {
		
		Location location = null;
		PIHeader piHeader = null;
		
		DocumentStatus open = documentStatusRepository.findByDocumentStatusCode("OPEN");
		
		
		//PI Header will have either location or partNumber
		if(piHeaderRequest.getLocationHashCode() != null){
			location = locationRepository.findByLocationHash(piHeaderRequest.getLocationHashCode());
			piHeader = piHeaderRepository.findByLocationCodeAndDocumentStatus(location.getLocationCode(),open);
		}else if(piHeaderRequest.getPartNumber() != null){
			piHeader = piHeaderRepository.findByPartNumberAndDocumentStatus(piHeaderRequest.getPartNumber(),open);
		}
		
		if(piHeader == null){
			piHeader = new PIHeader();
			if(location != null){
				piHeader.setLocationCode(location.getLocationCode());
			}
			piHeader.setPartNumber(piHeaderRequest.getPartNumber());
			piHeader.setActualCaseQty(piHeaderRequest.getBoxCount());
			piHeader.setDocumentStatus(open);
			piHeader.setSerialNotExist(0.0);
			piHeader.setIncorrectLocation(0.0);
			piHeader.setDateCreated(new Date());
			piHeader.setCreatedBy(piHeaderRequest.getCurrentUser());
		}else{
			piHeader.setActualCaseQty(piHeader.getActualCaseQty() + piHeaderRequest.getBoxCount());
		}
		   piHeader.setDateUpdated(new Date());
		   piHeader.setUpdatedBy(piHeaderRequest.getCurrentUser());
		  
		   
	    piHeaderRepository.save(piHeader);
	    
	    Boolean isBoxCountMatches = false;
	    if(location != null){
	    	List<InventoryMaster> inventorys = inventoryMasterRepository.findByCurrentLocation(location);
	    	
	    	int count  = piHeaderRequest.getBoxCount().intValue();
	    	InventoryStatus satus = inventoryStatusRepository.findByStatus("PI");
	    	//match box count
	    	if(inventorys.size() == count){
	    		//if box count matches close PI Header
	    		for(InventoryMaster inventory:inventorys){
	    			inventory.setStockDate(new Date());
	    			inventory.setLastUpdated(new Date());
	    			inventory.setLastUpdatedBy(piHeaderRequest.getCurrentUser());
	    			inventory.setInventoryStatus(satus);
	    			inventoryMasterRepository.save(inventory);
	    		}
	    		
	    		piHeader.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode("CLOSED"));
	    		
	    		isBoxCountMatches = true;
	    		
	    	}else{
	    		//if box count matches not matches 
	    		
	    		for(InventoryMaster inventory:inventorys){
	    			inventory.setCurrentLocation(locationRepository.findByLocationCode("SKTCHK"));
	    			inventory.setLocationCode("SKTCHK");
	    			inventory.setLastUpdated(new Date());
	    			inventory.setLastUpdatedBy(piHeaderRequest.getCurrentUser());
	    			inventory.setInventoryStatus(satus);
	    			inventoryMasterRepository.save(inventory);
	    		}
	    		
	    		isBoxCountMatches = false;
	    	}
	    }

    	
    	return isBoxCountMatches;
	}



	@Override
	public String processPI(ProcessRequest processRequest) {
		
		Location location = locationRepository.findByLocationHash(processRequest.getLocationHashCode());
		Part part = partRepository.findByPartNumber(processRequest.getPartNumber());
		DocumentStatus open = documentStatusRepository.findByDocumentStatusCode("OPEN");
		InventoryMaster inventory = inventoryMasterRepository.findBySerialReferenceAndPart(processRequest.getSerialReference(), part);
		String message;
		
		
		if(inventory == null){
			//if serial do not exist
			PIHeader piHeader = piHeaderRepository.findByLocationCodeAndDocumentStatus(location.getLocationCode(),open);
			piHeader.setSerialNotExist(piHeader.getSerialNotExist() + 1);
			piHeader.setDateUpdated(new Date());
			piHeader.setUpdatedBy(processRequest.getCurrentUser());
			piHeaderRepository.save(piHeader);
			
			message = "Invalid Serial";
		}else if(inventory.getCurrentLocation().getLocationCode().equalsIgnoreCase(location.getLocationCode())){
			
            //correct location
			
			recordTransaction(processRequest,inventory);
			
			
			inventory.setStockDate(new Date());
			inventory.setInventoryStatus(inventoryStatusRepository.findByStatus("OK"));
			inventory.setLastUpdated(new Date());
			inventory.setLastUpdatedBy(processRequest.getCurrentUser());
			inventoryMasterRepository.save(inventory);
			
			message = "Correct Location";
			
		}else{ 
			
            //incorrect location
			recordTransaction(processRequest,inventory);
			
			PIHeader piHeader = piHeaderRepository.findByLocationCodeAndDocumentStatus(location.getLocationCode(),open);
			piHeader.setIncorrectLocation(piHeader.getIncorrectLocation() + 1);
			piHeader.setDateUpdated(new Date());
			piHeader.setUpdatedBy(processRequest.getCurrentUser());
			piHeaderRepository.save(piHeader);
			
			message = "Correct Location is " + location.getLocationCode();
		}
		
		return message;
	}
	
	
	private Boolean recordTransaction(ProcessRequest processRequest,InventoryMaster inventory) {
			
			TransactionHistory transactionHistory = new TransactionHistory();
			
			transactionHistory.setFromLocationCode(inventory.getCurrentLocation().getLocationCode()); 
			transactionHistory.setToLocationCode(processRequest.getLocationCode()); 
			//transactionHistory.setToPlt(inventory.g); 
			//transactionHistory.setFromPlt(inventory.getPltNumber());
			//transactionHistory.setCountQty(txn.getCountQty());
			//transactionHistory.setCustomerReference(inventory.get);
			transactionHistory.setPartNumber(inventory.getPart().getPartNumber());
			transactionHistory.setRanOrOrder(inventory.getRanOrOrder());
			//transactionHistory.setReasonCode(processRequest.get);
			transactionHistory.setRequiresDecant(inventory.getRequiresDecant());
			transactionHistory.setRequiresInspection(inventory.getRequiresInspection());
			transactionHistory.setTxnQty(inventory.getInventoryQty()); 
			transactionHistory.setSerialReference(inventory.getSerialReference());
			transactionHistory.setShortCode(processRequest.getTransactionTypeId());
			//transactionHistory.setDocumentReference(inventory.getD);
			transactionHistory.setDateCreated(new Date());
			transactionHistory.setLastUpdated(new Date());
			transactionHistory.setLastUpdatedBy(processRequest.getCurrentUser());
			
			return transactionHistoryRepository.save(transactionHistory)!=null;
		}



	@Override
	public Boolean closePI(PIHeaderRequest piHeaderRequest) {
		
		DocumentStatus open = documentStatusRepository.findByDocumentStatusCode("OPEN");
		PIHeader piHeader = null;
		
		if(piHeaderRequest.getLocationHashCode() != null){
		    Location location = locationRepository.findByLocationHash(piHeaderRequest.getLocationHashCode());
			piHeader = piHeaderRepository.findByLocationCodeAndDocumentStatus(location.getLocationCode(),open);
		}else if(piHeaderRequest.getPartNumber() != null){
			piHeader = piHeaderRepository.findByPartNumberAndDocumentStatus(piHeaderRequest.getPartNumber(),open);
		}
		
		piHeader.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode("CLOSED"));
		piHeader.setDateUpdated(new Date());
		piHeader.setUpdatedBy(piHeaderRequest.getCurrentUser());
		
		return  piHeaderRepository.save(piHeader) != null;
	}



	@Override
	public Boolean createPIReasonCode(String shortCode, String desc, String createdBy) {
		
			PIReasonCode piReasonCode = new PIReasonCode();
			piReasonCode.setCreatedBy(createdBy);
			piReasonCode.setDateCreated(new Date());
			piReasonCode.setShortCode(shortCode);
			piReasonCode.setDescription(desc);
			piReasonCode.setUpdatedBy(createdBy);
			piReasonCode.setDateUpdated(new Date());
		
		return  piReasonCodeRepository.save(piReasonCode) != null;
	}
	
	
	@Override
	public Boolean updatePIReasonCode(Long id,String shortCode, String desc, String createdBy) {
		PIReasonCode piReasonCode = piReasonCodeRepository.findById(id);
		piReasonCode.setShortCode(shortCode);
		piReasonCode.setDescription(desc);
		piReasonCode.setUpdatedBy(createdBy);
		piReasonCode.setDateUpdated(new Date());
		
		return  piReasonCodeRepository.save(piReasonCode) != null;
	}



	@Override
	public Boolean closePIBody(PIBodyRequest piBodyRequest) {
		
        Location location = locationRepository.findByLocationHash(piBodyRequest.getLocationHashCode());
        PIHeader piHeader = piHeaderRepository.findById(piBodyRequest.getPiHeaderId());
		PIBody piBody = piBodyRepository.findByPiHeaderAndLocationCode(piHeader, location.getLocationCode());
		
		piBody.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode("CLOSED"));
		piBody.setDateUpdated(new Date());
		piBody.setUpdatedBy(piBodyRequest.getCurrentUser());
		
		return  piBodyRepository.save(piBody) != null;
	}



	
	@Override
	public Boolean createEditPIPartHeader(PIPartHeaderRequest piPartHeaderRequest) {
		PIHeader piHeader = null;
		
		DocumentStatus open = documentStatusRepository.findByDocumentStatusCode("OPEN");
		
		if(piPartHeaderRequest.getPiHeaderId() != null){
			piHeader = piHeaderRepository.findById(piPartHeaderRequest.getPiHeaderId());
		}else {
			piHeader = piHeaderRepository.findByPartNumberAndDocumentStatus(piPartHeaderRequest.getPartNumber(), open);
		}
	    
		
		if(piHeader == null){
			piHeader = new PIHeader();
		    piHeader.setPartNumber(piPartHeaderRequest.getPartNumber());
			piHeader.setDocumentStatus(open);
			piHeader.setDateCreated(new Date());
			piHeader.setCreatedBy(piPartHeaderRequest.getCurrentUser());
		}
		   PIReasonCode piReasonCode = piReasonCodeRepository.findById(piPartHeaderRequest.getPiReasonCodeId());
		   piHeader.setPiReasonCode(piReasonCode);
		   piHeader.setDateUpdated(new Date());
		   piHeader.setUpdatedBy(piPartHeaderRequest.getCurrentUser());
		  
		   
	    return piHeaderRepository.save(piHeader) != null;
	}
	
	
	@Override
	public Boolean createEditPILocationHeader(PILocationHeaderRequest piLocationHeaderRequest) {
		PIHeader piHeader = null;
		
		DocumentStatus open = documentStatusRepository.findByDocumentStatusCode("OPEN");
		
		if(piLocationHeaderRequest.getPiHeaderId() != null){
			piHeader = piHeaderRepository.findById(piLocationHeaderRequest.getPiHeaderId());
		}else {
			piHeader = piHeaderRepository.findByLocationCodeAndDocumentStatus(piLocationHeaderRequest.getLocationCode(), open);
		}
	    
		
		if(piHeader == null){
			piHeader = new PIHeader();
		    piHeader.setLocationCode(piLocationHeaderRequest.getLocationCode());
			piHeader.setDocumentStatus(open);
			piHeader.setDateCreated(new Date());
			piHeader.setCreatedBy(piLocationHeaderRequest.getCurrentUser());
		}
		   PIReasonCode piReasonCode = piReasonCodeRepository.findById(piLocationHeaderRequest.getPiReasonCodeId());
		   piHeader.setPiReasonCode(piReasonCode);
		   piHeader.setDateUpdated(new Date());
		   piHeader.setUpdatedBy(piLocationHeaderRequest.getCurrentUser());
		  
		   
	    return piHeaderRepository.save(piHeader) != null;
	}
	
	
	
}
