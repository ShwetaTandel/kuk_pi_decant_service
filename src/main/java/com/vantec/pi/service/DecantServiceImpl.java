package com.vantec.pi.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pi.business.DecantInfo;
import com.vantec.pi.entity.DecantBody;
import com.vantec.pi.entity.DecantHeader;
import com.vantec.pi.entity.DocumentStatus;
import com.vantec.pi.entity.InventoryMaster;
import com.vantec.pi.entity.Part;
import com.vantec.pi.model.DecantF2;
import com.vantec.pi.model.DecantRequest;
import com.vantec.pi.repository.DecantBodyRepository;
import com.vantec.pi.repository.DecantHeaderRepository;
import com.vantec.pi.repository.DocumentStatusRepository;
import com.vantec.pi.repository.InventoryMasterRepository;
import com.vantec.pi.repository.PartRepository;


@Service("DecantService")
public class DecantServiceImpl implements DecantService{
	
	private static Logger logger = LoggerFactory.getLogger(DecantServiceImpl.class);
	
	@Autowired
	DecantHeaderRepository decantHeaderRepository;
	
	@Autowired
	DecantBodyRepository decantBodyRepository;
	
	@Autowired
	DocumentStatusRepository documentStatusRepository;
	
	@Autowired
	InventoryMasterRepository inventoryMasterRepository;
	
	@Autowired
	PartRepository partRepository;
	
	@Autowired
	DecantInfo decantInfo;
	

	@SuppressWarnings("unused")
	@Override
	public String createDecantHeader(DecantRequest decantRequest) {
		
		String message = "Fail  -Failed";
		Boolean isRanOrderValid = true;
		Boolean isProductTypeValid = true;
		DocumentStatus openStatus = documentStatusRepository.findByDocumentStatusCode("OPEN");
		DecantHeader openDecantHeader = decantHeaderRepository.findByPartNumberAndDocumentStatus(decantRequest.getPartNumber(),openStatus);
		
		DecantBody decantBody = null;
		if(openDecantHeader != null){
			decantBody =  decantBodyRepository.findByDecantHeaderAndSerialReference(openDecantHeader,decantRequest.getSerialReference());
		}
		DecantHeader existingDecantHeader = null;
		logger.info("existing decantRequest id  " +  decantRequest.getDecantHeaderId());
		if(decantRequest.getDecantHeaderId() != null){
			existingDecantHeader = decantHeaderRepository.findById(decantRequest.getDecantHeaderId());
		}
		
		Long decantHeaderId = 0L;
		Part part = partRepository.findByPartNumber(decantRequest.getPartNumber());
		
		
		//messages
		
		if(decantBody != null && decantBody.getDecantStatus() > 1){
			message = "Fail  -Decant Body Decanted";
		}else if(existingDecantHeader != null && !existingDecantHeader.getPartNumber().equalsIgnoreCase(decantRequest.getPartNumber()) ){
			message = "Fail  -Serial Number is not correct";
		}
		
		
		//create
		if(((openDecantHeader == null) || (openDecantHeader != null && decantBody == null))  &&  (existingDecantHeader == null || existingDecantHeader.getDocumentStatus().getDocumentStatusCode().equalsIgnoreCase("OPEN")) ){
			logger.info("first ...");
			InventoryMaster inventoryToBeDecant = inventoryMasterRepository.findBySerialReferenceAndPartNumber(decantRequest.getSerialReference(), decantRequest.getPartNumber());
			
			
			//validate RanOrder 
			logger.info("before ran validaion " + inventoryToBeDecant + " yyyyy " + existingDecantHeader);
			if(inventoryToBeDecant!=null && existingDecantHeader!=null){
				logger.info("before calling validateRanOrder first");
				isRanOrderValid = decantInfo.validateRanOrder(inventoryToBeDecant,existingDecantHeader);
				//isProductTypeValid = decantInfo.validateProductType(inventoryToBeDecant, existingDecantHeader);
			}
			if(inventoryToBeDecant!=null && openDecantHeader!=null){
				logger.info("before calling validateRanOrder second");
				isRanOrderValid = decantInfo.validateRanOrder(inventoryToBeDecant,openDecantHeader);
				//isProductTypeValid = decantInfo.validateProductType(inventoryToBeDecant, openDecantHeader);
			}
			
			
			
			logger.info("isRanOrderValid  " + isRanOrderValid);
			
			if(inventoryToBeDecant == null){
				message = "Fail  -No Inventory";
			}else if(!isRanOrderValid){
				logger.info("ranorder not valid");
				message = "Fail  -RanOrder is not valid";
			}else if(!isProductTypeValid){
				logger.info("ranorder not valid");
				message = "Fail  -ProductType is not valid";
			}else{
				DecantBody decantBodyWithInventory = decantBodyRepository.findByInventoryMaster(inventoryToBeDecant);
				logger.info("inventoryToBeDecant.getCurrentLocation().getLocationType().getLocationTypeCode() ... " + inventoryToBeDecant.getCurrentLocation().getLocationType().getLocationTypeCode());
				
				if(decantBodyWithInventory!=null){
					message = "Fail  -Inventory already decanted";
				}else if(!(("DEC").equalsIgnoreCase(inventoryToBeDecant.getCurrentLocation().getLocationType().getLocationTypeCode()))){
					message = "Fail  -Inventory Not in Decant Location";
				}else if(decantBodyWithInventory == null && (("DEC").equalsIgnoreCase(inventoryToBeDecant.getCurrentLocation().getLocationType().getLocationTypeCode()))){
					openDecantHeader = decantInfo.createDecant(decantRequest,part,openStatus,openDecantHeader);
					message = openDecantHeader.getId().toString();
				}
			}
			
		} 
		
		DocumentStatus allocateStatus = documentStatusRepository.findByDocumentStatusCode("ALLOCATED");
		//DecantHeader allocatedDecantHeader = decantHeaderRepository.findByPartNumberAndDocumentStatus(decantRequest.getPartNumber(),allocateStatus);
		
		
		//process
		if(existingDecantHeader!=null) {
			
			decantBody =  decantBodyRepository.findByDecantHeaderAndSerialReference(existingDecantHeader,decantRequest.getSerialReference());
			
			logger.info("decant hedaer status  " + existingDecantHeader.getDocumentStatus().getDocumentStatusCode());
			logger.info("decant body status  " + decantBody.getDecantStatus());
			if(decantBody != null && decantBody.getDecantStatus() == 1 && existingDecantHeader.getDocumentStatus().getDocumentStatusCode().equalsIgnoreCase("ALLOCATED")){
				logger.info("second ...");
			
			
			List<DecantBody> decantBodyWithInventorys = decantBodyRepository.findByDecantHeaderAndInventoryMasterNotNull(existingDecantHeader);
			DecantBody decantBodyWithInventory = decantBodyWithInventorys.get(0);
			InventoryMaster inventoryToBeDecant = inventoryMasterRepository.findById(decantBodyWithInventory.getInventoryMaster().getId());
			
			InventoryMaster inventory = decantInfo.createInventoryMaster(decantRequest,part,decantBody,inventoryToBeDecant);
			decantInfo.createTransactionHistory(decantRequest,existingDecantHeader,inventory,inventoryToBeDecant);
			decantInfo.updateDecantBodyStatus(decantBody,decantRequest.getUserId(),2);
			
			Integer leftOverQty = 0;
			Integer zero = 0;
			for(DecantBody decantBodyWithInv:decantBodyWithInventorys){
				
				leftOverQty = decantInfo.updateInventoryToBeDecantedWithQty(decantBodyWithInv.getInventoryMaster(),inventory.getInventoryQty(),decantBodyWithInv,leftOverQty,decantRequest.getUserId());
				if(leftOverQty.compareTo(zero) == 0){
					break;
				}
			}
			
			}
			
		}
		
		//close
		if(existingDecantHeader!=null && existingDecantHeader.getDocumentStatus().getDocumentStatusCode().equalsIgnoreCase("ALLOCATED") && isRanOrderValid){
			decantHeaderId = existingDecantHeader.getId();
			message = existingDecantHeader.getId().toString();
			logger.info("refernce " +existingDecantHeader.getDocumentReference());
			//DecantBody openDecantbody = decantBodyRepository.findByDocumentReference(decantHeader.getDocumentReference());
			List<DecantBody> preDecantbody = decantBodyRepository.findAllByDocumentReferenceAndDecantStatus(existingDecantHeader.getDocumentReference(),0);
			List<DecantBody> postDecantbody = decantBodyRepository.findAllByDocumentReferenceAndDecantStatus(existingDecantHeader.getDocumentReference(),1);
			List<DecantBody> scannedDecantbody = decantBodyRepository.findAllByDocumentReferenceAndDecantStatus(existingDecantHeader.getDocumentReference(),2);
			logger.info("all size " + preDecantbody.size() + " " + postDecantbody.size() + " " + scannedDecantbody.size() );
			if(preDecantbody.size() >= 0 && postDecantbody.size() == 0 && scannedDecantbody.size() > 0){
				logger.info("close ...");
				decantInfo.closeDecant(decantHeaderId,decantRequest.getUserId());
				message = "OK  -"+existingDecantHeader.getDocumentReference() + " is Closed";
			}
		}
		
		
		logger.info("final message ... "+  message);
		
		return message;
		
	}


	@Override
	public Long f2Done(DecantF2 decantF2) {
		
		
		DecantHeader decantHeader = decantHeaderRepository.findById(decantF2.getDecantHeaderId());
		
		if(decantHeader.getDocumentStatus().getDocumentStatusCode().equalsIgnoreCase("CLOSED")){
			//return decantHeader.getId();
		}
		
		//
		logger.info("decantHeader " + decantHeader.getDocumentReference() + " NOT CLOSED");
		List<DecantBody> decantBodiesWithStatus1 = decantBodyRepository.findAllByDocumentReferenceAndDecantStatus(decantHeader.getDocumentReference(),1);
		decantBodyRepository.deleteInBatch(decantBodiesWithStatus1);
		
		List<DecantBody> decantBodies = decantBodyRepository.findAllByDocumentReference(decantHeader.getDocumentReference());
		
		logger.info("decantBodies " + decantBodies.size());
		
		
		//get sum of decant body qty
		Double totalQty = 0.0;
		Double eachQty = 0.0;
		Boolean isQtySame = true;
		for(DecantBody decantBody:decantBodies){
			logger.info("decantBodies " + decantBody.getId());
			logger.info("compare values " + eachQty + " " + decantBody.getQty());
            if (Double.compare(eachQty, decantBody.getQty()) != 0 && eachQty != 0) { 
            	isQtySame = false;
			}
			totalQty = totalQty + decantBody.getQty();
			eachQty = decantBody.getQty();
			
		}
		
		
		//if qty scanned is gearter than total qty
		if(Double.compare(decantF2.getQty(),totalQty.doubleValue()) == 1 ){
			logger.info("qty scanned is gearter than total qty");
			return 0L;
		}
	
		if(isQtySame && Double.compare(eachQty, decantF2.getQty())==0){
			logger.info("isQtySame " + isQtySame);
			for(DecantBody decantBody:decantBodies){
				decantInfo.updateDecantBodyStatus(decantBody,decantF2.getUserId(),1);
				decantInfo.updateInventoryMasterDecantFalse(decantBody.getInventoryMaster(),decantF2.getUserId());
				decantInfo.allocateDecantHeader(decantF2.getDecantHeaderId(), decantF2.getUserId());
			}
		}else{
			logger.info("totalQty " + totalQty);
			
			double qtyInEachBoxes = decantF2.getQty();
			double noOfBox = Math.ceil(totalQty / qtyInEachBoxes);
			double qtyInLastBox = totalQty % qtyInEachBoxes; //remainder
			
			logger.info("qtyInEachBoxes " + qtyInEachBoxes);
			logger.info("noOfBox " + noOfBox);
			logger.info("qtyInLastBox " + qtyInLastBox);
			
			if(qtyInLastBox==0){
				for(int i=1;i<=noOfBox;i++){
					decantInfo.createDecantBodyWithDecantSerials(decantHeader,decantF2,qtyInEachBoxes,i);
				}
			}else{
				for(int i=1;i<noOfBox;i++){
					decantInfo.createDecantBodyWithDecantSerials(decantHeader,decantF2,qtyInEachBoxes,i);
				}
				decantInfo.createDecantBodyWithDecantSerials(decantHeader,decantF2,qtyInLastBox,(int)noOfBox);
			}
			
			decantInfo.allocateDecantHeader(decantF2.getDecantHeaderId(), decantF2.getUserId());
			for(DecantBody decantBody:decantBodies){
				logger.info("getInventoryMaster " + decantBody.getInventoryMaster().getId());
				decantInfo.updateInventoryMasterDecantFalse(decantBody.getInventoryMaster(),decantF2.getUserId());
				
			}
		}

		
		
		return decantHeader.getId();
	
	}
	
	public static void main(String args[]){
		
		System.out.println(Double.compare(500.00,500.00));
		
		
	}
}
