package com.vantec.pi.service;

import com.vantec.pi.model.PIBodyRequest;
import com.vantec.pi.model.PIHeaderRequest;
import com.vantec.pi.model.PILocationHeaderRequest;
import com.vantec.pi.model.PIPartHeaderRequest;
import com.vantec.pi.model.ProcessRequest;

public interface PIService {

	public Boolean createUpdatePIHeader(PIHeaderRequest piHeaderRequest);

	public String processPI(ProcessRequest processRequest);

	public Boolean closePI(PIHeaderRequest piHeaderRequest);

	public Boolean createPIReasonCode(String shortCode, String desc, String createdBy);
	
	public Boolean updatePIReasonCode(Long id,String shortCode, String desc, String createdBy);

	public Boolean closePIBody(PIBodyRequest piBodyRequest);
	
	public Boolean createEditPIPartHeader(PIPartHeaderRequest piPartHeaderRequest);

	public Boolean createEditPILocationHeader(PILocationHeaderRequest piLocationHeaderRequest);
	
}
