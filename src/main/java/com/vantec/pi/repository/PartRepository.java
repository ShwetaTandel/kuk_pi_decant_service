package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.Part;

public interface PartRepository extends JpaRepository<Part, Serializable>{
	
	Part findByPartNumber(String partNumber);
	
}

