package com.vantec.pi.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.InventoryMaster;
import com.vantec.pi.entity.Location;
import com.vantec.pi.entity.Part;

public interface InventoryMasterRepository extends JpaRepository<InventoryMaster, Serializable>{

	List<InventoryMaster> findByCurrentLocation(Location location);
	
	InventoryMaster findBySerialReferenceAndPart(String serialReference,Part part);
	
	InventoryMaster findBySerialReferenceAndPartNumber(String serialReference,String partNumber);

	InventoryMaster findById(Long id);
	
}

