package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.ProductType;

public interface ProductTypeRepository extends JpaRepository<ProductType, Serializable>{
	
	ProductType findById(Long id);
}

