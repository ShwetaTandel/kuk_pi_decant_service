package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.InventoryStatus;

public interface InventoryStatusRepository extends JpaRepository<InventoryStatus, Serializable>{

	InventoryStatus findByStatus(String statusCode);
	
}

