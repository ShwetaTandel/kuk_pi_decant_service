package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.DocumentStatus;

public interface DocumentStatusRepository extends JpaRepository<DocumentStatus, Serializable>{

	DocumentStatus findByDocumentStatusCode(String statusCode);
	
}

