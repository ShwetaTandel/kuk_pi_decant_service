package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.PIDetail;

public interface PIDetailRepository extends JpaRepository<PIDetail, Serializable>{

	
	
}

