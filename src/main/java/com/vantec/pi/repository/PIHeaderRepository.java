package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.DocumentStatus;
import com.vantec.pi.entity.PIHeader;

public interface PIHeaderRepository extends JpaRepository<PIHeader, Serializable>{

	PIHeader findByLocationCodeAndDocumentStatus(String locationCode,DocumentStatus docStatus);
	
	PIHeader findByPartNumberAndDocumentStatus(String partNumber,DocumentStatus docStatus);
	
	PIHeader findById(Long id);
	
}

