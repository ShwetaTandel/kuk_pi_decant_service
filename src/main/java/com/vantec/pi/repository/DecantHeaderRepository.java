package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.DecantHeader;
import com.vantec.pi.entity.DocumentStatus;

public interface DecantHeaderRepository extends JpaRepository<DecantHeader, Serializable>{

	DecantHeader findByPartNumberAndDocumentStatus(String partNumber, DocumentStatus status);
	
	DecantHeader findById(Long id);
}

