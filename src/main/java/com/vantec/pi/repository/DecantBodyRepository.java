package com.vantec.pi.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.pi.entity.DecantBody;
import com.vantec.pi.entity.DecantHeader;
import com.vantec.pi.entity.InventoryMaster;

public interface DecantBodyRepository extends JpaRepository<DecantBody, Serializable>{

	DecantBody findByDecantHeaderAndSerialReference(DecantHeader decantHeader, String serialReference);

	@Query("select d from DecantBody d where d.documentReference='documentReference' and d.decantStatus in ('0','1')")
	DecantBody findByDocumentReferenceAndStatus(String documentReference);

	DecantBody findByDocumentReference(String documentReference);
	
	List<DecantBody> findAllByDocumentReference(String documentReference);

	List<DecantBody> findAllByDocumentReferenceAndDecantStatus(String documentReference, int i);

	DecantBody findById(Long id);
	
	DecantBody findByInventoryMaster(InventoryMaster inventory);

	List<DecantBody> findByDecantHeaderAndInventoryMasterNotNull(DecantHeader decantHeader);
}

