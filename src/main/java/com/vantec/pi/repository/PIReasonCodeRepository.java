package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.PIReasonCode;

public interface PIReasonCodeRepository extends JpaRepository<PIReasonCode, Serializable>{

	PIReasonCode findByShortCode(String shortCode);
	
	PIReasonCode findById(Long id);
	
}

