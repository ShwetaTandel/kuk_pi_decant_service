package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.PIBody;
import com.vantec.pi.entity.PIHeader;

public interface PIBodyRepository extends JpaRepository<PIBody, Serializable>{

	PIBody findByPiHeaderAndLocationCode(PIHeader piHeadder,String locationCode);
	
}

