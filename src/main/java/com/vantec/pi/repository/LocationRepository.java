package com.vantec.pi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pi.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Serializable>{

	Location findByLocationCode(String locationCode);
	
	Location findByLocationHash(String hashCode);
	
}

