package com.vantec.pi.controller;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.pi.model.DecantF2;
import com.vantec.pi.model.DecantRequest;
import com.vantec.pi.service.DecantService;

@RestController
@RequestMapping("/")
public class DecantController {
	
	private static Logger logger = LoggerFactory.getLogger(DecantController.class);
	
	
	@Autowired
	DecantService decantService;
	
	
	
    @RequestMapping(value = "/createDecantHeader", method = RequestMethod.POST)
	public ResponseEntity<String> createDecantHeader(@RequestBody DecantRequest decantRequest) throws ParseException {
    	
    	logger.info("createDecantHeader called.. part "  +  decantRequest.getPartNumber());
    	logger.info("createDecantHeader called.. serial "  +  decantRequest.getSerialReference());
			try{
				
				return new ResponseEntity<String>(decantService.createDecantHeader(decantRequest), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("DecantRequest Values : "   +  "PartNumber : " + decantRequest.getPartNumber() +
		    			                                     "SerialReference : " + decantRequest.getSerialReference());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
    
    
    
    @RequestMapping(value = "/f2Done", method = RequestMethod.POST)
	public ResponseEntity<Long> f2Done(@RequestBody DecantF2 decantF2) throws ParseException {
    	
    	logger.info("f2Done called.. " + decantF2.getDecantHeaderId());
			try{
				return new ResponseEntity<Long>(decantService.f2Done(decantF2), HttpStatus.OK );
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "decantHeaderId : " + decantF2.getDecantHeaderId() +
		    			                      "Qty : " + decantF2.getQty());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
    


}
