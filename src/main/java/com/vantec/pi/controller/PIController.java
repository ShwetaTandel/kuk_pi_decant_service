package com.vantec.pi.controller;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.pi.model.PIBodyRequest;
import com.vantec.pi.model.PIHeaderRequest;
import com.vantec.pi.model.PILocationHeaderRequest;
import com.vantec.pi.model.PIPartHeaderRequest;
import com.vantec.pi.model.ProcessRequest;
import com.vantec.pi.service.PIService;

@RestController
@RequestMapping("/")
public class PIController {
	
	private static Logger logger = LoggerFactory.getLogger(PIController.class);
	
	
	@Autowired
	PIService piService;
	
	
	//create from gun
    @RequestMapping(value = "/createUpdatePIHeader", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createUpdatePIHeader(@RequestBody PIHeaderRequest piHeaderRequest) throws ParseException {
    	
    	logger.info("createUpdatePIHeader called..");
			try{
				
				return new ResponseEntity<Boolean>(piService.createUpdatePIHeader(piHeaderRequest), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("PIHeaderRequest Values : "   +  "LocationCode : " + piHeaderRequest.getLocationCode() +
		    			                                      "LocationHashCode : " + piHeaderRequest.getLocationHashCode() + 
		    			                                      "userId : " + piHeaderRequest.getCurrentUser() + 
		    			                                      "PiHeaderId : " + piHeaderRequest.getPiHeaderId() +
		    			                                      "BoxCount : " + piHeaderRequest.getBoxCount());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
    
    
    //create PI Part from UI
    @RequestMapping(value = "/createEditPIPartHeader", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createEditPIPartHeader(@RequestBody PIPartHeaderRequest piPartHeaderRequest) throws ParseException {
			
			try{
				logger.info("createEditPIPartHeader called..");
				return new ResponseEntity<Boolean>(piService.createEditPIPartHeader(piPartHeaderRequest), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("PIPartHeaderRequest Values : "   +  "PartNumber : " + piPartHeaderRequest.getPartNumber() + 
		    			                                          "currentUser : " + piPartHeaderRequest.getCurrentUser());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
    
    //create PI Location from UI
    @RequestMapping(value = "/createEditPILocationHeader", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createEditPILocationHeader(@RequestBody PILocationHeaderRequest piLocationHeaderRequest) throws ParseException {
    	
			try{
				logger.info("createEditPIPartHeader called..");
				return new ResponseEntity<Boolean>(piService.createEditPILocationHeader(piLocationHeaderRequest), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("PILocationHeaderRequest Values : "   +  "LocationCode : " + piLocationHeaderRequest.getLocationCode() + 
		    			                                              "currentUser : " + piLocationHeaderRequest.getCurrentUser());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
    
    
    @RequestMapping(value = "/"
    		+ "", method = RequestMethod.POST)
	public ResponseEntity<String> processPI(@RequestBody ProcessRequest processRequest) throws ParseException {
    	
			return new ResponseEntity<String>(piService.processPI(processRequest), HttpStatus.OK);
	}
    
    
    @RequestMapping(value = "/closePI", method = RequestMethod.POST)
   	public ResponseEntity<Boolean> closePI(@RequestBody PIHeaderRequest piHeaderRequest) throws ParseException {
    	
    	
   			return new ResponseEntity<Boolean>(piService.closePI(piHeaderRequest), HttpStatus.OK);
   	}
    
    @RequestMapping(value = "/closePIBody", method = RequestMethod.POST)
   	public ResponseEntity<Boolean> closePIBody(@RequestBody PIBodyRequest piBodyRequest) throws ParseException {
    	
   			return new ResponseEntity<Boolean>(piService.closePIBody(piBodyRequest), HttpStatus.OK);
   	}
    
    @RequestMapping(value = "/createPIReasonCode", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createPIReasonCode(@RequestParam String shortCode,@RequestParam String desc,@RequestParam String createdBy) throws ParseException {
			
			try{
				logger.info("createPIReasonCode called..");
				return new ResponseEntity<Boolean>(piService.createPIReasonCode(shortCode,desc,createdBy), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error(" Values : "   +  "shortCode : " + shortCode + 
		    								   "desc : " + desc + 
		    			                       "createdBy : " + createdBy);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
    
    @RequestMapping(value = "/updatePIReasonCode", method = RequestMethod.POST)
   	public ResponseEntity<Boolean> updatePIReasonCode(@RequestParam Long id,@RequestParam String shortCode,@RequestParam String desc,@RequestParam String createdBy) throws ParseException {
   			
   			try{
				logger.info("updatePIReasonCode called..");
				return new ResponseEntity<Boolean>(piService.updatePIReasonCode(id,shortCode,desc,createdBy), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error(" Values : "   +  "shortCode : " + shortCode + 
		    								   "desc : " + desc + 
		    			                       "createdBy : " + createdBy);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
   	}

}
