package com.vantec.pi.command;

public interface CustomValueGeneratorCommand<T> {

	public T generate(Long id,String name) ;
}
