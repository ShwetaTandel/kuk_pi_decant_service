package com.vantec.pi.business;

import com.vantec.pi.entity.DecantBody;
import com.vantec.pi.entity.DecantHeader;
import com.vantec.pi.entity.DocumentStatus;
import com.vantec.pi.entity.InventoryMaster;
import com.vantec.pi.entity.Part;
import com.vantec.pi.model.DecantF2;
import com.vantec.pi.model.DecantRequest;

public interface DecantInfo {

	public DecantHeader createDecant(DecantRequest decantRequest, Part part, DocumentStatus status, DecantHeader decantHeader);

	public InventoryMaster createInventoryMaster(DecantRequest decantRequest,Part part,DecantBody decantBody, InventoryMaster inventoryToBeDecant);

	public void createTransactionHistory(DecantRequest decantRequest, DecantHeader decantHeader, InventoryMaster inventory, InventoryMaster inventoryToBeDecant);

	public void updateDecantBodyStatus(DecantBody decantBody,String userId,int status);

	public void updateInventoryMaster(DecantRequest decantRequest);

	public void closeDecant(Long decantHeaderId, String string);

	public void createDecantBodyWithDecantSerials(DecantHeader decantHeader,DecantF2 decantF2,Double qty,int i);

	public void updateInventoryMasterWithQtyZero(InventoryMaster inventory);

	public Integer updateInventoryToBeDecantedWithQty(InventoryMaster inventoryToBeDecant,Integer qty, DecantBody decantBodyWithInventory, Integer leftOverQty,String userId);

	public Boolean validateRanOrder(InventoryMaster inventoryToBeDecant, DecantHeader existingDecantHeader);

	void allocateDecantHeader(Long decantHeaderId, String userId);

	public void updateInventoryMasterDecantFalse(InventoryMaster inventoryMaster,String user);

	Boolean validateProductType(InventoryMaster inventoryToBeDecant, DecantHeader existingDecantHeader);

	
	
}
