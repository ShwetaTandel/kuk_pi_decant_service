package com.vantec.pi.business;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pi.command.CustomValueGeneratorCommand;
import com.vantec.pi.command.DocumentReferenceGenerator;
import com.vantec.pi.entity.DecantBody;
import com.vantec.pi.entity.DecantHeader;
import com.vantec.pi.entity.DocumentStatus;
import com.vantec.pi.entity.InventoryMaster;
import com.vantec.pi.entity.InventoryStatus;
import com.vantec.pi.entity.Location;
import com.vantec.pi.entity.Part;
import com.vantec.pi.entity.ProductType;
import com.vantec.pi.entity.TransactionHistory;
import com.vantec.pi.model.DecantF2;
import com.vantec.pi.model.DecantRequest;
import com.vantec.pi.repository.DecantBodyRepository;
import com.vantec.pi.repository.DecantHeaderRepository;
import com.vantec.pi.repository.DocumentStatusRepository;
import com.vantec.pi.repository.InventoryMasterRepository;
import com.vantec.pi.repository.InventoryStatusRepository;
import com.vantec.pi.repository.LocationRepository;
import com.vantec.pi.repository.PartRepository;
import com.vantec.pi.repository.ProductTypeRepository;
import com.vantec.pi.repository.TransactionHistoryRepository;

@Service("decantInfo")
public class DecantInfoImpl implements DecantInfo {
	
	private static Logger logger = LoggerFactory.getLogger(DecantInfoImpl.class);
	
	@Autowired
	DocumentStatusRepository documentStatusRepository;
	
	@Autowired
	PartRepository partRepository;
	
	@Autowired
	DecantHeaderRepository decantHeaderRepository;
	
	@Autowired
	DecantBodyRepository decantBodyRepository;
	
	@Autowired
	InventoryMasterRepository inventoryMasterRepository;
	
	@Autowired
	LocationRepository locationRepository;
	
	@Autowired
	InventoryStatusRepository inventoryStatusRepository;
	
	@Autowired
	TransactionHistoryRepository transactionHistoryRepository;
	
	@Autowired
	ProductTypeRepository productTypeRepository;


	@Override
	public DecantHeader createDecant(DecantRequest decantRequest,Part part,DocumentStatus status,DecantHeader decantHeader) {
		
		if(decantHeader == null){
			decantHeader = createDecantHeader(decantRequest, part, status);
		}
		
		createDecantBody(decantRequest,decantHeader);
		
		return decantHeader;
	}


	private DecantHeader createDecantHeader(DecantRequest decantRequest, Part part, DocumentStatus status) {
		DecantHeader decantHeader = new DecantHeader();
		decantHeader.setCreatedBy(decantRequest.getUserId());
		decantHeader.setDateCreated(new Date());
		decantHeader.setDateUpdated(new Date());
		decantHeader.setDocumentStatus(status);
		decantHeader.setPart(part);
		decantHeader.setPartNumber(decantRequest.getPartNumber());
		decantHeader.setUpdatedBy(decantRequest.getUserId());
		decantHeader.setVendorId(part.getVendor().getId());
		decantHeaderRepository.save(decantHeader);
		
		
		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
		String documentReference = customValueGeneratorCommand.generate(decantHeader.getId(), "DEC");
		decantHeader.setDocumentReference(documentReference);
		decantHeaderRepository.save(decantHeader);
		
		return decantHeader;
				
	}
	
	
	public DecantBody createDecantBody(DecantRequest decantRequest,DecantHeader decantHeader){
		
		
		InventoryMaster inventoryMaster = inventoryMasterRepository.findBySerialReferenceAndPartNumber(decantRequest.getSerialReference(), decantRequest.getPartNumber());
		Part part = partRepository.findByPartNumber(decantRequest.getPartNumber());
				
				
		DecantBody decantBody = new DecantBody(); 
		decantBody.setCreatedBy(decantRequest.getUserId());
		decantBody.setDateCreated(new Date());
		decantBody.setDateUpdated(new Date());
		decantBody.setDecantHeader(decantHeader);
		decantBody.setDecantStatus(0);
		decantBody.setDocumentReference(decantHeader.getDocumentReference());
		decantBody.setSerialReference(decantRequest.getSerialReference());
		decantBody.setUpdatedBy(decantRequest.getUserId());
		decantBody.setInventoryMaster(inventoryMaster);
		decantBody.setQty(Double.valueOf(inventoryMaster.getInventoryQty())/part.getConversionFactor());
		decantBody.setTtRanOrder(inventoryMaster.getRanOrOrder());
		decantBodyRepository.save(decantBody);
		
		return decantBody;
		
	}
	
	
	public void createDecantBodyWithDecantSerials(DecantHeader decantHeader,DecantF2 decantF2,Double qty,int i){
		
		DecantBody decantBody = new DecantBody(); 
		decantBody.setCreatedBy(decantF2.getUserId());
		decantBody.setDateCreated(new Date());
		decantBody.setDateUpdated(new Date());
		decantBody.setDecantHeader(decantHeader);
		decantBody.setDecantStatus(1);
		decantBody.setDocumentReference(decantHeader.getDocumentReference());
		decantBody.setSerialReference(decantHeader.getDocumentReference()+ "SE" + i);
		decantBody.setUpdatedBy(decantF2.getUserId());
		decantBody.setQty(qty);
		decantBodyRepository.save(decantBody);
		
	}


	@Override
	public InventoryMaster createInventoryMaster(DecantRequest decantRequest,Part part,DecantBody decantBody,InventoryMaster inventoryToBeDecant) {
		
		logger.info("createInventoryMaster CALLED ");
		
	 InventoryMaster inventoryMaster = inventoryMasterRepository.findBySerialReferenceAndPartNumber(decantRequest.getSerialReference(), part.getPartNumber());
	 
	 String ranOrder = null;
	 if(("NO-RAN").equalsIgnoreCase(inventoryToBeDecant.getRanOrOrder()) || ("").equalsIgnoreCase(inventoryToBeDecant.getRanOrOrder())){
		 ranOrder = "NO-RAN";
	 }else{
		 ranOrder="DEC-RAN";
	 }
	
	 
	 if(inventoryMaster == null){
	     inventoryMaster = new InventoryMaster();
	     Location location = locationRepository.findByLocationCode(inventoryToBeDecant.getLocationCode());
	     inventoryMaster.setCurrentLocation(location);
	     inventoryMaster.setDateCreated(new Date());
	     inventoryMaster.setAllocatedQty(0);
	     inventoryMaster.setInventoryQty(decantBody.getQty().intValue()*part.getConversionFactor());
	     inventoryMaster.setAvailableQty(decantBody.getQty().intValue()*part.getConversionFactor());
	     InventoryStatus inventoryStatus = inventoryStatusRepository.findByStatus("OK");
	     inventoryMaster.setInventoryStatus(inventoryStatus);
	     inventoryMaster.setLastUpdated(new Date());
	     inventoryMaster.setLastUpdatedBy(decantRequest.getUserId());
	     inventoryMaster.setLocationCode(location.getLocationCode());
	     inventoryMaster.setPart(part);
	     inventoryMaster.setPartNumber(part.getPartNumber());
	     inventoryMaster.setRanOrOrder(ranOrder);//this
	     inventoryMaster.setRequiresDecant(false);//this
	     inventoryMaster.setRequiresInspection(false);
	     inventoryMaster.setSerialReference(decantRequest.getSerialReference());
	     inventoryMaster.setStockDate(new Date());
	     inventoryMaster.setRecordedMissing(false);
	     inventoryMaster.setConversionFactor(part.getConversionFactor());
	     inventoryMaster.setVersion(0L);
	     inventoryMaster.setProductTypeId(inventoryToBeDecant.getProductTypeId());
	     inventoryMaster = inventoryMasterRepository.save(inventoryMaster);
	 }else{
		 inventoryMaster.setLastUpdated(new Date());
		 inventoryMaster.setLastUpdatedBy(decantRequest.getUserId());
		 inventoryMaster.setRequiresDecant(false);
		 inventoryMaster = inventoryMasterRepository.save(inventoryMaster);
	 }
	 
	 return inventoryMaster;
		
	}


	@Override
	public void createTransactionHistory(DecantRequest decantRequest,DecantHeader decantHeader,InventoryMaster inventory,InventoryMaster inventoryToBeDecant) {
		
		
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActive(false);
		transactionHistory.setAssociatedDocumentReference(decantHeader.getDocumentReference());
		transactionHistory.setComment("Decant Transaction");
		transactionHistory.setDateCreated(new Date());
		transactionHistory.setFromLocationCode(inventory.getLocationCode());
		transactionHistory.setLastUpdated(new Date());
		transactionHistory.setLastUpdatedBy(decantRequest.getUserId());
		transactionHistory.setPartNumber(decantRequest.getPartNumber());
		transactionHistory.setRanOrOrder(inventoryToBeDecant.getRanOrOrder());
		transactionHistory.setRequiresDecant(inventoryToBeDecant.getRequiresDecant());
		transactionHistory.setRequiresInspection(inventoryToBeDecant.getRequiresInspection());
		transactionHistory.setSerialReference(decantRequest.getSerialReference());
		transactionHistory.setShortCode("DECANT");
		transactionHistory.setTransactionReferenceCode("DECANT");
		transactionHistory.setTxnQty(inventory.getInventoryQty());
		if(inventoryToBeDecant.getProductTypeId()!=null){
			ProductType product = productTypeRepository.findById(inventoryToBeDecant.getProductTypeId().longValue());
			transactionHistory.setProductTypeCode(product.getProductTypeCode());
		}
		transactionHistoryRepository.save(transactionHistory);
	}


	@Override
	public void updateDecantBodyStatus(DecantBody decantBody,String userId,int status) {
		
		decantBody = decantBodyRepository.findById(decantBody.getId());
		decantBody.setDecantStatus(status);
		decantBody.setDateUpdated(new Date());
		decantBody.setUpdatedBy(userId);
		decantBodyRepository.save(decantBody);
		
	}


	@Override
	public void updateInventoryMaster(DecantRequest decantRequest) {
		
		InventoryMaster inventory = inventoryMasterRepository.findBySerialReferenceAndPartNumber(decantRequest.getSerialReference(), decantRequest.getPartNumber());
		inventory.setLastUpdated(new Date());
		inventory.setLastUpdatedBy(decantRequest.getUserId());
		inventory.setRequiresDecant(true);
		inventoryMasterRepository.save(inventory);
		
	}


	@Override
	public void closeDecant(Long decantHeaderId,String userId) {
		
		DecantHeader decantHeader = decantHeaderRepository.findById(decantHeaderId);
		decantHeader.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode("CLOSED"));
		decantHeader.setDateUpdated(new Date());
		decantHeader.setUpdatedBy(userId);
		decantHeaderRepository.save(decantHeader);
		
	}
	
	
	@Override
	public void allocateDecantHeader(Long decantHeaderId,String userId) {
		
		DecantHeader decantHeader = decantHeaderRepository.findById(decantHeaderId);
		decantHeader.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode("ALLOCATED"));
		decantHeader.setDateUpdated(new Date());
		decantHeader.setUpdatedBy(userId);
		decantHeaderRepository.save(decantHeader);
		
	}



	@Override
	public void updateInventoryMasterWithQtyZero(InventoryMaster inventory) {
		inventory.setInventoryQty(0);
		inventoryMasterRepository.save(inventory);
		
	}


	@Override
	public Integer updateInventoryToBeDecantedWithQty(InventoryMaster inventoryToBeDecant,Integer qty,DecantBody decantBodyWithInventory,Integer leftOverQty,String userId) {
		Integer zero = 0;
		if(leftOverQty.compareTo(zero) > 0){
			qty = leftOverQty;
		}
		System.out.println("updateInventoryToBeDecantedWithQty  " + inventoryToBeDecant.getInventoryQty() + " " + qty);
		if(Integer.compare(inventoryToBeDecant.getInventoryQty(),qty) == 0){
			System.out.println("delete inventory");
			decantBodyWithInventory.setInventoryMaster(null);
			DecantBody decantBody = decantBodyRepository.save(decantBodyWithInventory);
			createTransactionHistoryDECOUT(inventoryToBeDecant,userId);
			if(decantBody.getDecantStatus()==0){
				inventoryMasterRepository.delete(inventoryToBeDecant);
			}
			leftOverQty = 0 ;
		}else if(Integer.compare(inventoryToBeDecant.getInventoryQty(),qty) > 0){
			System.out.println("substract inventory");
			inventoryToBeDecant.setInventoryQty(inventoryToBeDecant.getInventoryQty() - qty);
			inventoryToBeDecant.setLastUpdated(new Date());
			inventoryMasterRepository.save(inventoryToBeDecant);
			leftOverQty = 0;
		}else if(Integer.compare(inventoryToBeDecant.getInventoryQty(),qty) < 0){
			System.out.println("delete from first inventory");
			decantBodyWithInventory.setInventoryMaster(null);
			DecantBody decantBody = decantBodyRepository.save(decantBodyWithInventory);
			createTransactionHistoryDECOUT(inventoryToBeDecant,userId);
			if(decantBody.getDecantStatus()==0){
				inventoryMasterRepository.delete(inventoryToBeDecant);
			}
			leftOverQty = qty - inventoryToBeDecant.getInventoryQty();
		}
		
		
	    System.out.println("leftOverQty " + leftOverQty);
		return leftOverQty;


      }

	//Both Ran should be either NO-RAN or some RAN 
	//Cant mix with NO-RAN

	@Override
	public Boolean validateRanOrder(InventoryMaster inventoryToBeDecant, DecantHeader existingDecantHeader) {
		
		Boolean isRanOrderValid = true;
		DecantBody existingDecanyBody = decantBodyRepository.findAllByDocumentReference(existingDecantHeader.getDocumentReference()).get(0);
		String existingRanOrder = existingDecanyBody.getTtRanOrder();
		
		if(existingRanOrder.equalsIgnoreCase("NO-RAN") && !inventoryToBeDecant.getRanOrOrder().equalsIgnoreCase("NO-RAN")){
			System.out.println("first condition making it false");
			isRanOrderValid = false;
		}
		if(!existingRanOrder.equalsIgnoreCase("NO-RAN") && inventoryToBeDecant.getRanOrOrder().equalsIgnoreCase("NO-RAN")){
			System.out.println("second condition making it false");
			isRanOrderValid = false;
		}
		
	    return isRanOrderValid;
	}
	
	@Override
	public Boolean validateProductType(InventoryMaster inventoryToBeDecant, DecantHeader existingDecantHeader) {
		
		Boolean isProductTypeValid = true;
		DecantBody existingDecanyBody = decantBodyRepository.findAllByDocumentReference(existingDecantHeader.getDocumentReference()).get(0);
		if(existingDecanyBody!=null){
			Integer  decantProductTypeId = existingDecanyBody.getInventoryMaster().getProductTypeId();
			Integer  inventoryProductTypeId = inventoryToBeDecant.getProductTypeId();
		
			if(decantProductTypeId.intValue()!=inventoryProductTypeId.intValue()){
			isProductTypeValid = false;
			}}
		
	    return isProductTypeValid;
	}
	
	private void createTransactionHistoryDECOUT(InventoryMaster inventoryToBeDecant,String userId){
		
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActive(false);
		//transactionHistory.setAssociatedDocumentReference(decantHeader.getDocumentReference());
		transactionHistory.setComment("Decant Transaction");
		transactionHistory.setDateCreated(new Date());
		transactionHistory.setFromLocationCode(inventoryToBeDecant.getLocationCode());
		transactionHistory.setLastUpdated(new Date());
		transactionHistory.setLastUpdatedBy(userId);
		transactionHistory.setPartNumber(inventoryToBeDecant.getPartNumber());
		transactionHistory.setRanOrOrder(inventoryToBeDecant.getRanOrOrder());
		transactionHistory.setRequiresDecant(inventoryToBeDecant.getRequiresDecant());
		transactionHistory.setRequiresInspection(inventoryToBeDecant.getRequiresInspection());
		transactionHistory.setSerialReference(inventoryToBeDecant.getSerialReference());
		transactionHistory.setShortCode("DEC_OUT");
		transactionHistory.setTransactionReferenceCode("DEC_OUT");
		transactionHistory.setTxnQty(inventoryToBeDecant.getInventoryQty());
		if(inventoryToBeDecant.getProductTypeId()!=null){
			ProductType product = productTypeRepository.findById(inventoryToBeDecant.getProductTypeId().longValue());
			transactionHistory.setProductTypeCode(product.getProductTypeCode());
		}
		transactionHistoryRepository.save(transactionHistory);
		
	}


	@Override
	public void updateInventoryMasterDecantFalse(InventoryMaster inventoryMaster,String user) {
		
		InventoryMaster inventory = inventoryMasterRepository.findById(inventoryMaster.getId());
		inventory.setRequiresDecant(false);
		inventory.setLastUpdated(new Date());
		inventory.setLastUpdatedBy(user);
		inventoryMasterRepository.save(inventoryMaster);
		
		
	}
	

}
