package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "inventoryMaster")
public class InventoryMaster implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currentLocationId", referencedColumnName = "id")
	private Location currentLocation;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventoryStatusId", referencedColumnName = "id")
	private InventoryStatus inventoryStatus;
	
	@Column(name = "locationCode")
	private String locationCode;
	
	@Column(name = "ranOrOrder")
	private String ranOrOrder;
	
	@Column(name = "serialReference")
	private String serialReference;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "inventoryQty")
	private Integer inventoryQty;
	
	@Column(name = "allocatedQty")
	private Integer allocatedQty;
	
	@Column(name = "availableQty")
	private Integer availableQty;
	
	@Column(name = "productTypeId")
	private Integer productTypeId;

	@Column(name = "stockDate")
	private Date stockDate;
	
	@Column(name = "requiresDecant")
	private Boolean requiresDecant;
	
	@Column(name = "requiresInspection")
	private Boolean requiresInspection;
	
	@Column(name = "recordedMissing")
	private Boolean recordedMissing;
	
	@Column(name = "conversionFactor")
	private Integer conversionFactor;

	@Column(name = "dateCreated")
	private Date dateCreated;

	@Column(name = "lastUpdated")
	private Date lastUpdated;

	@Column(name = "lastUpdatedBy")
	private String lastUpdatedBy;

	public InventoryMaster() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public Date getStockDate() {
		return stockDate;
	}

	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}

	public String getRanOrOrder() {
		return ranOrOrder;
	}

	public void setRanOrOrder(String ranOrOrder) {
		this.ranOrOrder = ranOrOrder;
	}

	public Integer getInventoryQty() {
		return inventoryQty;
	}

	public void setInventoryQty(Integer inventoryQty) {
		this.inventoryQty = inventoryQty;
	}

	public Boolean getRequiresDecant() {
		return requiresDecant;
	}

	public void setRequiresDecant(Boolean requiresDecant) {
		this.requiresDecant = requiresDecant;
	}

	public Boolean getRequiresInspection() {
		return requiresInspection;
	}

	public void setRequiresInspection(Boolean requiresInspection) {
		this.requiresInspection = requiresInspection;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public InventoryStatus getInventoryStatus() {
		return inventoryStatus;
	}

	public void setInventoryStatus(InventoryStatus inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Integer getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(Integer allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public Boolean getRecordedMissing() {
		return recordedMissing;
	}

	public void setRecordedMissing(Boolean recordedMissing) {
		this.recordedMissing = recordedMissing;
	}

	public Integer getConversionFactor() {
		return conversionFactor;
	}

	public void setConversionFactor(Integer conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Integer getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}

	public Integer getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Integer productTypeId) {
		this.productTypeId = productTypeId;
	}
    
	
	
	

	

}
