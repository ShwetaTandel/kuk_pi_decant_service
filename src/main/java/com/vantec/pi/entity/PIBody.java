package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "piBody")
public class PIBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "locationCode")
	private String locationCode;
	
	@Column(name = "actualQty")
	private Double actualQty;
	
	@Column(name = "expectedQty")
	private Double expectedQty;
	
	@Column(name = "difference")
	private Double difference;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "piHeaderId", referencedColumnName = "id")
	private PIHeader piHeader;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;
	
	@Column(name = "serialNotExist")
	private Double serialNotExist;
	
	@Column(name = "incorrectLocation")
	private Double incorrectLocation;
	
	@Column(name = "pickingLeft")
	private Integer pickingLeft;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Double getDifference() {
		return difference;
	}

	public void setDifference(Double difference) {
		this.difference = difference;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public Double getActualQty() {
		return actualQty;
	}

	public void setActualQty(Double actualQty) {
		this.actualQty = actualQty;
	}

	public Double getExpectedQty() {
		return expectedQty;
	}

	public void setExpectedQty(Double expectedQty) {
		this.expectedQty = expectedQty;
	}

	public PIHeader getPiHeader() {
		return piHeader;
	}

	public void setPiHeader(PIHeader piHeader) {
		this.piHeader = piHeader;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public Double getSerialNotExist() {
		return serialNotExist;
	}

	public void setSerialNotExist(Double serialNotExist) {
		this.serialNotExist = serialNotExist;
	}

	public Double getIncorrectLocation() {
		return incorrectLocation;
	}

	public void setIncorrectLocation(Double incorrectLocation) {
		this.incorrectLocation = incorrectLocation;
	}

	public Integer getPickingLeft() {
		return pickingLeft;
	}

	public void setPickingLeft(Integer pickingLeft) {
		this.pickingLeft = pickingLeft;
	}
	
	
}
