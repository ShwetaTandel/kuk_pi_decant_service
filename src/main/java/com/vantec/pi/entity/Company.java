package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@NamedQueries(  
	    {  
	@NamedQuery(
		    name="getDefaultSettingsForCompany",
		    query="SELECT c FROM Company c WHERE c.companyName = :companyName"
		)
  }  
)


@Entity
@Table(name = "company")
public class Company implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "companyName")
	private String companyName;

	@Column(name = "shortCode")
	private String shortCode;

	@Column(name = "address")
	private String address;
	
	@Column(name = "interfaceInput")
	private String interfaceInput;

	@Column(name = "interfaceOutput")
	private String interfaceOutput;
	
	@Column(name = "interfaceActive")
	private Boolean interfaceActive;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;
	
	@Column(name = "mixedPartStorage")
	private Boolean mixedPartStorage;

	@Column(name = "qtyDecimalPlace")
	private Integer qtyDecimalPlace;

	@Column(name = "virtualPalletLbl")
	private Boolean virtualPalletLbl;

	@Column(name = "minBoxQty")
	private Integer minBoxQty;

	@Column(name = "maxBoxQty")
	private Integer maxBoxQty;

	@Column(name = "minPartNumberLength")
	private Integer minPartNumberLength;

	@Column(name = "maxPartNumberLength")
	private Integer maxPartNumberLength;

	@Column(name = "minRanNumberLength")
	private Integer minRanNumberLength;

	@Column(name = "maxRanNumberLength")
	private Integer maxRanNumberLength;

	@Column(name = "minSerialNumberLength")
	private Integer minSerialNumberLength;

	@Column(name = "maxSerialNumberLength")
	private Integer maxSerialNumberLength;
	
	@Column(name = "interfaceSplitConfMsg")
	private String interfaceSplitConfMsg;
	
	@Column(name = "toQa")
	private Boolean toQA;

	@Column(name = "toDecant")
	private Boolean toDecant;
	
	@Column(name = "flagOverbookings")
	private Boolean flagOverbookings;
	
	@Column(name = "autoAllocate")
	private Boolean autoAllocate;
	
	@Column(name = "strictSerialPick")
	private Boolean strictSerialPick ;
	
	@Column(name = "piStrictSerial")
	private Boolean piStrictSerial ;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public Company() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInterfaceInput() {
		return interfaceInput;
	}

	public void setInterfaceInput(String interfaceInput) {
		this.interfaceInput = interfaceInput;
	}

	public String getInterfaceOutput() {
		return interfaceOutput;
	}

	public void setInterfaceOutput(String interfaceOutput) {
		this.interfaceOutput = interfaceOutput;
	}

	public Boolean getInterfaceActive() {
		return interfaceActive;
	}

	public void setInterfaceActive(Boolean interfaceActive) {
		this.interfaceActive = interfaceActive;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Boolean getMixedPartStorage() {
		return mixedPartStorage;
	}

	public void setMixedPartStorage(Boolean mixedPartStorage) {
		this.mixedPartStorage = mixedPartStorage;
	}

	public Integer getQtyDecimalPlace() {
		return qtyDecimalPlace;
	}

	public void setQtyDecimalPlace(Integer qtyDecimalPlace) {
		this.qtyDecimalPlace = qtyDecimalPlace;
	}

	public Boolean getVirtualPalletLbl() {
		return virtualPalletLbl;
	}

	public void setVirtualPalletLbl(Boolean virtualPalletLbl) {
		this.virtualPalletLbl = virtualPalletLbl;
	}

	public Integer getMinBoxQty() {
		return minBoxQty;
	}

	public void setMinBoxQty(Integer minBoxQty) {
		this.minBoxQty = minBoxQty;
	}

	public Integer getMaxBoxQty() {
		return maxBoxQty;
	}

	public void setMaxBoxQty(Integer maxBoxQty) {
		this.maxBoxQty = maxBoxQty;
	}

	public Integer getMinPartNumberLength() {
		return minPartNumberLength;
	}

	public void setMinPartNumberLength(Integer minPartNumberLength) {
		this.minPartNumberLength = minPartNumberLength;
	}

	public Integer getMaxPartNumberLength() {
		return maxPartNumberLength;
	}

	public void setMaxPartNumberLength(Integer maxPartNumberLength) {
		this.maxPartNumberLength = maxPartNumberLength;
	}

	public Integer getMinRanNumberLength() {
		return minRanNumberLength;
	}

	public void setMinRanNumberLength(Integer minRanNumberLength) {
		this.minRanNumberLength = minRanNumberLength;
	}

	public Integer getMaxRanNumberLength() {
		return maxRanNumberLength;
	}

	public void setMaxRanNumberLength(Integer maxRanNumberLength) {
		this.maxRanNumberLength = maxRanNumberLength;
	}

	public Integer getMinSerialNumberLength() {
		return minSerialNumberLength;
	}

	public void setMinSerialNumberLength(Integer minSerialNumberLength) {
		this.minSerialNumberLength = minSerialNumberLength;
	}

	public Integer getMaxSerialNumberLength() {
		return maxSerialNumberLength;
	}

	public void setMaxSerialNumberLength(Integer maxSerialNumberLength) {
		this.maxSerialNumberLength = maxSerialNumberLength;
	}

	public String getInterfaceSplitConfMsg() {
		return interfaceSplitConfMsg;
	}

	public void setInterfaceSplitConfMsg(String interfaceSplitConfMsg) {
		this.interfaceSplitConfMsg = interfaceSplitConfMsg;
	}

	public Boolean getToQA() {
		return toQA;
	}

	public void setToQA(Boolean toQA) {
		this.toQA = toQA;
	}

	public Boolean getToDecant() {
		return toDecant;
	}

	public void setToDecant(Boolean toDecant) {
		this.toDecant = toDecant;
	}

	public Boolean getFlagOverbookings() {
		return flagOverbookings;
	}

	public void setFlagOverbookings(Boolean flagOverbookings) {
		this.flagOverbookings = flagOverbookings;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getAutoAllocate() {
		return autoAllocate;
	}

	public void setAutoAllocate(Boolean autoAllocate) {
		this.autoAllocate = autoAllocate;
	}

	public Boolean getStrictSerialPick() {
		return strictSerialPick;
	}

	public void setStrictSerialPick(Boolean strictSerialPick) {
		this.strictSerialPick = strictSerialPick;
	}

	public Boolean getPiStrictSerial() {
		return piStrictSerial;
	}

	public void setPiStrictSerial(Boolean piStrictSerial) {
		this.piStrictSerial = piStrictSerial;
	}
	

}
