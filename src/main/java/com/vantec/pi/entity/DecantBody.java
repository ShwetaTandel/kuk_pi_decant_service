package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "decantBody")
public class DecantBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "documentReference")
	private String documentReference;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "decantHeaderId", referencedColumnName = "id")
	private DecantHeader decantHeader;
	
	@Column(name = "ttRanOrder")
	private String ttRanOrder;
	
	@Column(name = "serialReference")
	private String serialReference;
	
	@Column(name = "qty")
	private Double qty;
	
	@Column(name = "decantStatus")
	private Integer decantStatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventoryMasterId", referencedColumnName = "id")
	private InventoryMaster inventoryMaster;

	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public DecantHeader getDecantHeader() {
		return decantHeader;
	}

	public void setDecantHeader(DecantHeader decantHeader) {
		this.decantHeader = decantHeader;
	}

	public String getTtRanOrder() {
		return ttRanOrder;
	}

	public void setTtRanOrder(String ttRanOrder) {
		this.ttRanOrder = ttRanOrder;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public InventoryMaster getInventoryMaster() {
		return inventoryMaster;
	}

	public void setInventoryMaster(InventoryMaster inventoryMaster) {
		this.inventoryMaster = inventoryMaster;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public Integer getDecantStatus() {
		return decantStatus;
	}

	public void setDecantStatus(Integer decantStatus) {
		this.decantStatus = decantStatus;
	}
    
    
    
}
