package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "locationRack",
       uniqueConstraints = {@UniqueConstraint(columnNames={"rackCode"})} )
public class LocationRack implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "rackCode")
	private String rackCode;

	@Column(name = "rackDescription")
	private String rackDescription;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "aisleId", referencedColumnName = "id")
	private LocationAisle aisle;
	
	@Column(name = "fillingPriority")
	private Long fillingPriority;
	
	@Column(name = "priorityRowColumn")
	private String priorityRowColumn;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public LocationRack() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRackCode() {
		return rackCode;
	}

	public void setRackCode(String rackCode) {
		this.rackCode = rackCode;
	}

	public String getRackDescription() {
		return rackDescription;
	}

	public void setRackDescription(String rackDescription) {
		this.rackDescription = rackDescription;
	}
    
	public LocationAisle getAisle() {
		return aisle;
	}

	public void setAisle(LocationAisle aisle) {
		this.aisle = aisle;
	}
	
	public Long getFillingPriority() {
		return fillingPriority;
	}

	public void setFillingPriority(Long fillingPriority) {
		this.fillingPriority = fillingPriority;
	}

	public String getPriorityRowColumn() {
		return priorityRowColumn;
	}

	public void setPriorityRowColumn(String priorityRowColumn) {
		this.priorityRowColumn = priorityRowColumn;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
