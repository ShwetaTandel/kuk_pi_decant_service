package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries(  
	    {  
	@NamedQuery(
		    name="findPartByPartNumber",
		    query="SELECT p FROM Part p WHERE p.partNumber like :partNumber"
		)
  }  
)

@Entity
@Table(name = "part")
public class Part implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "partNumber")
	private String partNumber;

	@Column(name = "effectiveFrom")
	private Date effectiveFrom;

	@Column(name = "effectiveTo")
	private Date effectiveTo;

	@Column(name = "partDescription")
	private String partDescription;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vendorId", referencedColumnName = "id")
	private Vendor vendor;

	@Column(name = "jisSupplyGroup")
	private String jisSupplyGroup;
	
	@Column(name = "conversionFactor")
	private Integer conversionFactor;
	
	@Column(name = "requiresDecant")
	private Boolean requiresDecant;


	@Column(name = "wiCode")
	private String wiCode;

	@Column(name = "dateCreated")
	private Date dateCreated;

	public Part() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public String getPartDescription() {
		return partDescription;
	}

	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getJisSupplyGroup() {
		return jisSupplyGroup;
	}

	public void setJisSupplyGroup(String jisSupplyGroup) {
		this.jisSupplyGroup = jisSupplyGroup;
	}

	public String getWiCode() {
		return wiCode;
	}

	public void setWiCode(String wiCode) {
		this.wiCode = wiCode;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Integer getConversionFactor() {
		return conversionFactor;
	}

	public void setConversionFactor(Integer conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	public Boolean getRequiresDecant() {
		return requiresDecant;
	}

	public void setRequiresDecant(Boolean requiresDecant) {
		this.requiresDecant = requiresDecant;
	}
    
	
}
