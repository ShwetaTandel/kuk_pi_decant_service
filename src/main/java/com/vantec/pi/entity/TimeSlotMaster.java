package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "timeSlotMaster")
public class TimeSlotMaster implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "timeSlot")
	private Date timeSlot;
	
	@Column(name = "destination")
	private String destination;
	
	@Column(name = "orderType")
	private String orderType;
	
	@Column(name = "sqlQuery")
	private String sqlQuery;
	
	@Column(name = "workingDays")
	private String workingDays;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "pickTime")
	private Date pickTime;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	

	public TimeSlotMaster() {
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Date getTimeSlot() {
		return timeSlot;
	}



	public void setTimeSlot(Date timeSlot) {
		this.timeSlot = timeSlot;
	}



	public String getDestination() {
		return destination;
	}



	public void setDestination(String destination) {
		this.destination = destination;
	}



	public String getOrderType() {
		return orderType;
	}



	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}



	public String getSqlQuery() {
		return sqlQuery;
	}



	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}



	public String getWorkingDays() {
		return workingDays;
	}



	public void setWorkingDays(String workingDays) {
		this.workingDays = workingDays;
	}



	public Date getPickTime() {
		return pickTime;
	}



	public void setPickTime(Date pickTime) {
		this.pickTime = pickTime;
	}



	public String getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public Date getDateCreated() {
		return dateCreated;
	}



	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public Date getDateUpdated() {
		return dateUpdated;
	}



	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	
	
}
