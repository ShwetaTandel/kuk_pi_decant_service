package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "locationZoneType",
       uniqueConstraints = {@UniqueConstraint(columnNames={"zoneTypeCode"})} )
public class LocationZoneType implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "zoneTypeCode")
	private String zoneTypeCode;

	@Column(name = "zoneTypeDescription")
	private String zoneTypeDescription;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public LocationZoneType() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZoneTypeCode() {
		return zoneTypeCode;
	}

	public void setZoneTypeCode(String zoneTypeCode) {
		this.zoneTypeCode = zoneTypeCode;
	}

	public String getZoneTypeDescription() {
		return zoneTypeDescription;
	}

	public void setZoneTypeDescription(String zoneTypeDescription) {
		this.zoneTypeDescription = zoneTypeDescription;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
