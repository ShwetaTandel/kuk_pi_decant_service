package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@NamedQueries(  
	    {  
	@NamedQuery(
		    name="getBodyFromHeaderAndPart",
		    query="SELECT b FROM ReceiptBody b WHERE b.receiptHeader = :receiptHeader and b.partNumber = :partNumber"
		),
	@NamedQuery(
		    name="deleteReceiptBody",
		    query="delete from ReceiptBody where documentReference = :documentReference"
		)
	  }
)

@Entity
@Table(name = "receiptBody")
@JsonIgnoreProperties(ignoreUnknown=true)
public class ReceiptBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "documentReference")
	private String documentReference;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiptHeaderId", referencedColumnName = "id")
	private ReceiptHeader receiptHeader;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@Column(name = "partNumber")
	private String partNumber;

	@Column(name = "advisedQty")
	private Double advisedQty;

	@Column(name = "receivedQty")
	private Double receivedQty;

	@Column(name = "difference")
	private Double difference;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@OneToMany(mappedBy = "receiptBody")
	private List<ReceiptDetail> receiptDetails = new ArrayList<ReceiptDetail>();

	public ReceiptBody() {
	}

	public List<ReceiptDetail> getReceiptDetails() {
		return receiptDetails;
	}

	public void setReceiptDetails(List<ReceiptDetail> receiptDetails) {
		this.receiptDetails = receiptDetails;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public ReceiptHeader getReceiptHeader() {
		return receiptHeader;
	}

	public void setReceiptHeader(ReceiptHeader receiptHeader) {
		this.receiptHeader = receiptHeader;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	public Double getAdvisedQty() {
		return advisedQty;
	}

	public void setAdvisedQty(Double advisedQty) {
		this.advisedQty = advisedQty;
	}

	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}

	public Double getDifference() {
		return difference;
	}

	public void setDifference(Double difference) {
		this.difference = difference;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
