package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@NamedQueries(  
	    {  
	@NamedQuery(
		    name="validateFromLocationByTransaction",
		    query="SELECT t FROM TransactionLocationType t WHERE t.location = :location and t.validTransactionIdFromLocation = :transaction"
		),
	@NamedQuery(
		    name="validateToLocationByTransaction",
		    query="SELECT t FROM TransactionLocationType t WHERE t.location = :location and t.validTransactionIdToLocation = :transaction"
		)
  }  
)

@Entity
@Table(name = "transactionLocationType")
public class TransactionLocationType implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "validTransactionIdFromLocation", referencedColumnName = "id")
	private TransactionType validTransactionIdFromLocation;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "validTransactionIdToLocation", referencedColumnName = "id")
	private TransactionType validTransactionIdToLocation;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locationId", referencedColumnName = "id")
	private Location location;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;
	
	public TransactionLocationType() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TransactionType getValidTransactionIdFromLocation() {
		return validTransactionIdFromLocation;
	}

	public void setValidTransactionIdFromLocation(TransactionType validTransactionIdFromLocation) {
		this.validTransactionIdFromLocation = validTransactionIdFromLocation;
	}

	public TransactionType getValidTransactionIdToLocation() {
		return validTransactionIdToLocation;
	}

	public void setValidTransactionIdToLocation(TransactionType validTransactionIdToLocation) {
		this.validTransactionIdToLocation = validTransactionIdToLocation;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
	
	
	
	
	

}
