package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transactionHistory")
public class TransactionHistory implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "associatedDocumentReference")
	private String associatedDocumentReference;
	
	@Column(name = "comment")
	private String comment;
	
	@Column(name = "fromLocationCode")
	private String fromLocationCode;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdated")
	private Date lastUpdated;
	
	@Column(name = "lastUpdatedBy")
	private String lastUpdatedBy;
	
	@Column(name = "parentTag")
	private String parentTag;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "ranOrOrder")
	private String ranOrOrder;
	
	@Column(name = "reasonCode")
	private String reasonCode;
	
	@Column(name = "requiresDecant")
	private Boolean requiresDecant;
	
	@Column(name = "requiresInspection")
	private Boolean requiresInspection;
	
	@Column(name = "serialReference")
	private String serialReference;
	
	@Column(name = "shortCode")
	private String shortCode;
	
	@Column(name = "sourceTag")
	private String sourceTag;
	
	@Column(name = "stockDisposition")
	private String stockDisposition;
	
	@Column(name = "toLocationCode")
	private String toLocationCode;
	
	@Column(name = "transactionReferenceCode")
	private String transactionReferenceCode;
	
	@Column(name = "productTypeCode")
	private String productTypeCode;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "txnQty")
	private Integer txnQty;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAssociatedDocumentReference() {
		return associatedDocumentReference;
	}

	public void setAssociatedDocumentReference(String associatedDocumentReference) {
		this.associatedDocumentReference = associatedDocumentReference;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFromLocationCode() {
		return fromLocationCode;
	}

	public void setFromLocationCode(String fromLocationCode) {
		this.fromLocationCode = fromLocationCode;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getParentTag() {
		return parentTag;
	}

	public void setParentTag(String parentTag) {
		this.parentTag = parentTag;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getRanOrOrder() {
		return ranOrOrder;
	}

	public void setRanOrOrder(String ranOrOrder) {
		this.ranOrOrder = ranOrOrder;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public Boolean getRequiresDecant() {
		return requiresDecant;
	}

	public void setRequiresDecant(Boolean requiresDecant) {
		this.requiresDecant = requiresDecant;
	}

	public Boolean getRequiresInspection() {
		return requiresInspection;
	}

	public void setRequiresInspection(Boolean requiresInspection) {
		this.requiresInspection = requiresInspection;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSourceTag() {
		return sourceTag;
	}

	public void setSourceTag(String sourceTag) {
		this.sourceTag = sourceTag;
	}

	public String getStockDisposition() {
		return stockDisposition;
	}

	public void setStockDisposition(String stockDisposition) {
		this.stockDisposition = stockDisposition;
	}

	public String getToLocationCode() {
		return toLocationCode;
	}

	public void setToLocationCode(String toLocationCode) {
		this.toLocationCode = toLocationCode;
	}

	public String getTransactionReferenceCode() {
		return transactionReferenceCode;
	}

	public void setTransactionReferenceCode(String transactionReferenceCode) {
		this.transactionReferenceCode = transactionReferenceCode;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getTxnQty() {
		return txnQty;
	}

	public void setTxnQty(Integer txnQty) {
		this.txnQty = txnQty;
	}

	public String getProductTypeCode() {
		return productTypeCode;
	}

	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}
	
	
	
	
	
	
	

}
