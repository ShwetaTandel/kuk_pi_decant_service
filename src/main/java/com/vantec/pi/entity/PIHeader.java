package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "piHeader")
public class PIHeader implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "locationCode")
	private String locationCode;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "actualCaseQty")
	private Double actualCaseQty;
	
	@Column(name = "expectedCaseQty")
	private Double expectedCaseQty;
	
	@Column(name = "difference")
	private Double difference;
	
	@Column(name = "serialNotExist")
	private Double serialNotExist;
	
	@Column(name = "incorrectLocation")
	private Double incorrectLocation;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "piReasonCodeId", referencedColumnName = "id")
	private PIReasonCode piReasonCode;
	
	@Column(name = "pickingLeft")
	private Integer pickingLeft;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Double getActualCaseQty() {
		return actualCaseQty;
	}

	public void setActualCaseQty(Double actualCaseQty) {
		this.actualCaseQty = actualCaseQty;
	}

	public Double getExpectedCaseQty() {
		return expectedCaseQty;
	}

	public void setExpectedCaseQty(Double expectedCaseQty) {
		this.expectedCaseQty = expectedCaseQty;
	}

	public Double getDifference() {
		return difference;
	}

	public void setDifference(Double difference) {
		this.difference = difference;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public Double getSerialNotExist() {
		return serialNotExist;
	}

	public void setSerialNotExist(Double serialNotExist) {
		this.serialNotExist = serialNotExist;
	}

	public Double getIncorrectLocation() {
		return incorrectLocation;
	}

	public void setIncorrectLocation(Double incorrectLocation) {
		this.incorrectLocation = incorrectLocation;
	}

	public PIReasonCode getPiReasonCode() {
		return piReasonCode;
	}

	public void setPiReasonCode(PIReasonCode piReasonCode) {
		this.piReasonCode = piReasonCode;
	}

	public Integer getPickingLeft() {
		return pickingLeft;
	}

	public void setPickingLeft(Integer pickingLeft) {
		this.pickingLeft = pickingLeft;
	}
	
}
