package com.vantec.pi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pickDetail")
public class PickDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "inventoryMasterId", referencedColumnName = "id")
	private InventoryMaster inventoryMaster;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pickBodyId", referencedColumnName = "id")
	private PickBody pickBody;
	
	@Column(name = "documentReference")
	private String documentReference;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "serialReference")
	private String serialReference;
	
	@Column(name = "serialReferenceScanned")
	private String serialReferenceScanned;
	
	@Column(name = "locationCode")
	private String locationCode;
	
	@Column(name = "qtyAllocated")
	private Double qtyAllocated;
	
	@Column(name = "qtyScanned")
	private Double qtyScanned;
	
	@Column(name = "lineNo")
	private Integer lineNo;
	
	@Column(name = "pickPriority")
	private Long pickPriority;
	
	@Column(name = "processed")
	private Boolean processed;
	
	@Column(name = "inUseBy")
	private String inUseBy;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public InventoryMaster getInventoryMaster() {
		return inventoryMaster;
	}

	public void setInventoryMaster(InventoryMaster inventoryMaster) {
		this.inventoryMaster = inventoryMaster;
	}

	public PickBody getPickBody() {
		return pickBody;
	}

	public void setPickBody(PickBody pickBody) {
		this.pickBody = pickBody;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public Double getQtyAllocated() {
		return qtyAllocated;
	}

	public void setQtyAllocated(Double qtyAllocated) {
		this.qtyAllocated = qtyAllocated;
	}

	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public Long getPickPriority() {
		return pickPriority;
	}

	public void setPickPriority(Long pickPriority) {
		this.pickPriority = pickPriority;
	}

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public String getInUseBy() {
		return inUseBy;
	}

	public void setInUseBy(String inUseBy) {
		this.inUseBy = inUseBy;
	}

	public String getSerialReferenceScanned() {
		return serialReferenceScanned;
	}

	public void setSerialReferenceScanned(String serialReferenceScanned) {
		this.serialReferenceScanned = serialReferenceScanned;
	}

	public Double getQtyScanned() {
		return qtyScanned;
	}

	public void setQtyScanned(Double qtyScanned) {
		this.qtyScanned = qtyScanned;
	}
	
    
}
