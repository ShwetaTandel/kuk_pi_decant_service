package com.vantec.pi.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DecantF2 {

	
	Long decantHeaderId;
	double qty;
	String userId;
	public Long getDecantHeaderId() {
		return decantHeaderId;
	}
	public void setDecantHeaderId(Long decantHeaderId) {
		this.decantHeaderId = decantHeaderId;
	}
	public double getQty() {
		return qty;
	}
	public void setQty(double qty) {
		this.qty = qty;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
    
	
	
	
	
	
	
	
}
