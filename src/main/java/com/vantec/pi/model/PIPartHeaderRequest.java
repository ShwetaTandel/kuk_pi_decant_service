package com.vantec.pi.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PIPartHeaderRequest {
	
	Long piHeaderId;
	String partNumber;
	Long piReasonCodeId;
	String currentUser;
	
	public Long getPiHeaderId() {
		return piHeaderId;
	}
	public void setPiHeaderId(Long piHeaderId) {
		this.piHeaderId = piHeaderId;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Long getPiReasonCodeId() {
		return piReasonCodeId;
	}
	public void setPiReasonCodeId(Long piReasonCodeId) {
		this.piReasonCodeId = piReasonCodeId;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	
	
	
	
}
