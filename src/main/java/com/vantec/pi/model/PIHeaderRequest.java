package com.vantec.pi.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PIHeaderRequest {
	
	//PIHeader will get created either with location or part
	String locationHashCode;
	String locationCode;
	String partNumber;
	
	Long piHeaderId;
	
	Double boxCount;
	String currentUser;
	
	Long piReasonCodeId;  //from UI
	
    
	public String getLocationHashCode() {
		return locationHashCode;
	}
	public void setLocationHashCode(String locationHashCode) {
		this.locationHashCode = locationHashCode;
	}
	public Double getBoxCount() {
		return boxCount;
	}
	public void setBoxCount(Double boxCount) {
		this.boxCount = boxCount;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Long getPiReasonCodeId() {
		return piReasonCodeId;
	}
	public void setPiReasonCodeId(Long piReasonCodeId) {
		this.piReasonCodeId = piReasonCodeId;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public Long getPiHeaderId() {
		return piHeaderId;
	}
	public void setPiHeaderId(Long piHeaderId) {
		this.piHeaderId = piHeaderId;
	}
	
	
	
	
}
