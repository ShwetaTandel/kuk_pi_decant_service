package com.vantec.pi.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PIBodyRequest {
	
	//Request to close PI Body
	Long piHeaderId;
	String locationHashCode;
	String currentUser;
	
	public Long getPiHeaderId() {
		return piHeaderId;
	}
	public void setPiHeaderId(Long piHeaderId) {
		this.piHeaderId = piHeaderId;
	}
	public String getLocationHashCode() {
		return locationHashCode;
	}
	public void setLocationHashCode(String locationHashCode) {
		this.locationHashCode = locationHashCode;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	
	
	
	
	
}
