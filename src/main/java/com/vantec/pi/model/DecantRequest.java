package com.vantec.pi.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DecantRequest {

	
	String partNumber;
	String serialReference;
	String userId;
	
	Long decantHeaderId;
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getDecantHeaderId() {
		return decantHeaderId;
	}
	public void setDecantHeaderId(Long decantHeaderId) {
		this.decantHeaderId = decantHeaderId;
	}
    
	
	
	
	
	
	
	
}
