package com.vantec.pi.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PILocationHeaderRequest {
	
	Long piHeaderId;
	String locationCode;
	Long piReasonCodeId;
	String currentUser;
	
	public Long getPiHeaderId() {
		return piHeaderId;
	}
	public void setPiHeaderId(Long piHeaderId) {
		this.piHeaderId = piHeaderId;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public Long getPiReasonCodeId() {
		return piReasonCodeId;
	}
	public void setPiReasonCodeId(Long piReasonCodeId) {
		this.piReasonCodeId = piReasonCodeId;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	
	
	
	
}
